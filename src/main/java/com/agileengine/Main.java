package com.agileengine;

import com.agileengine.impl.ComparisonLogicImpl;

import java.io.File;
import java.util.Scanner;

/**
 * Created by Dmitriy on 14.01.2016.
 * For AgileEngine
 */
public class Main {

    public static void main(String... args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter full path to 1 image: ");
        System.out.flush();
        File image1 = new File(scanner.nextLine());
        if (image1.isFile()) {
            System.out.print("Enter full path to 2 image: ");
            System.out.flush();
            File image2 = new File(scanner.nextLine());
            if (image2.isFile()) {
                ComparisonLogic comparisonLogic = new ComparisonLogicImpl();
                comparisonLogic.compareImages(image1, image2);
            } else {
                printError();
            }
        } else {
            printError();
        }
    }

    private static void printError() {
        System.out.println("Specify correct path to image");
    }
}
