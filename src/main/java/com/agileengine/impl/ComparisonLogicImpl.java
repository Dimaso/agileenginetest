package com.agileengine.impl;

import com.agileengine.ComparisonLogic;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dmitriy on 14.01.2016.
 */
public class ComparisonLogicImpl implements ComparisonLogic {


    private final int DIFF_THRESHOLD = 10;
    private final int PIXEL_THRESHOLD = 3;
    private int matchedArea = 1;

    @Override
    public void compareImages(File image1, File image2) {
        try {
            BufferedImage imageToCompare1 = ImageIO.read(image1);
            BufferedImage imageToCompare2 = ImageIO.read(image2);
            if (isImagesSizeEqual(imageToCompare1, imageToCompare2)) {
                int[][] colorDifference = calculateColorDifference(imageToCompare1, imageToCompare2);
                List<Rectangle> rectangles = findAreasWithDifference(colorDifference);
                highlightDifference(imageToCompare1, imageToCompare2, rectangles);
            } else {
                System.out.println("Image sizes are not equal");
            }
        } catch (IOException e) {
            printError(e);
        }

    }

    private List<Rectangle> findAreasWithDifference(int[][] colorDifference) {
        List<Rectangle> result = new ArrayList<Rectangle>();
        for (int row = 0; row < colorDifference.length; row++) {
            for (int col = 0; col < colorDifference[0].length; col++) {
                if (colorDifference[row][col] == 1) {
                    Rectangle rectangle = findAreaBorders(row, col, colorDifference);
                    if (rectangle.width > PIXEL_THRESHOLD  && rectangle.height > PIXEL_THRESHOLD) {
                        result.add(rectangle);
                    }
                }
            }
        }
        return result;
    }

    private Rectangle findAreaBorders(int x, int y, int[][] colorDifference) {
        boolean foundInZone = false;
        int maxWidth = colorDifference.length;
        int maxHeight = colorDifference[0].length;
        int width = 1, height = 1;
        do {
            foundInZone = false;

            int zoneX = x, zoneY = y, zoneWidth = width, zoneHeight = height;

            if (zoneX - PIXEL_THRESHOLD < 0) zoneX = 0;
            else zoneX -= PIXEL_THRESHOLD;

            if (zoneY - PIXEL_THRESHOLD < 0) zoneY = 0;
            else zoneY -= PIXEL_THRESHOLD;

            if (x + zoneWidth + PIXEL_THRESHOLD < maxWidth) zoneWidth += 2*PIXEL_THRESHOLD;
            else zoneWidth = maxWidth - zoneX;

            if (y + zoneHeight + PIXEL_THRESHOLD < maxHeight) zoneHeight += 2*PIXEL_THRESHOLD;
            else zoneHeight = maxHeight - zoneY;

            List<ComparisonLogicImpl.Point> foundedInZone = new LinkedList<>();
            for (int i=zoneX; i<zoneX+zoneWidth; i++)
                for (int j=zoneY; j<zoneY+zoneHeight; j++)
                    if (colorDifference[i][j]==1)
                        foundedInZone.add(this.new Point(i,j));

            if (!foundedInZone.isEmpty()) {
                foundInZone = true;

                int right = x + width, bottom = y + height;

                int newX = foundedInZone.stream().map(p -> p.x).min(Comparator.<Integer>naturalOrder()).get();
                if (newX < x) x = newX;

                int newRight = foundedInZone.stream().map(p -> p.x).max(Comparator.<Integer>naturalOrder()).get();
                if (newRight > right) width = newRight - x;
                else width = right - x;

                int newY = foundedInZone.stream().map(p -> p.y).min(Comparator.<Integer>naturalOrder()).get();
                if (newY < y) y = newY;

                int newBottom = foundedInZone.stream().map(p -> p.y).max(Comparator.<Integer>naturalOrder()).get();
                if (newBottom > bottom) height = newBottom - y;
                else height = bottom - y;

                for (Point point : foundedInZone)
                    colorDifference[point.x][point.y] = 0;
            }
        } while (foundInZone);

        return new Rectangle(x, y, width, height);
    }

    class Point {
        int x;
        int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    private void highlightDifference(BufferedImage imageToCompare1,
                                     BufferedImage imageToCompare2,
                                     List<Rectangle> rectangles) {

        Graphics2D imageGraphics1 = imageToCompare1.createGraphics();
        imageGraphics1.setColor(Color.RED);
        Graphics2D imageGraphics2 = imageToCompare2.createGraphics();
        imageGraphics2.setColor(Color.RED);
        for (Rectangle rectangle : rectangles) {
            imageGraphics1.draw(rectangle);
            imageGraphics2.draw(rectangle);
        }
        imageGraphics1.dispose();
        imageGraphics2.dispose();
        File outputfile1 = new File("imageCompare1.png");
        File outputfile2 = new File("imageCompare2.png");
        try {
            ImageIO.write(imageToCompare1, "png", outputfile1);
            ImageIO.write(imageToCompare2, "png", outputfile2);
        } catch (IOException e) {
            printError(e);
        }
    }

    private int[][] calculateColorDifference(BufferedImage imageToCompare1, BufferedImage imageToCompare2) {
        int width = imageToCompare1.getWidth();
        int height = imageToCompare1.getHeight();
        int pixelColor1;
        int pixelColor2;
        int[][] result = new int[width][height];
        for (int row = 0; row < width; row++) {
            for (int col = 0; col < height; col++) {
                pixelColor1 = imageToCompare1.getRGB(row, col);
                pixelColor2 = imageToCompare2.getRGB(row, col);
                if (diffPercentage(pixelColor1, pixelColor2) > DIFF_THRESHOLD) {
                    result[row][col] = 1;
                }
            }
        }
        return result;
    }

    private int diffPercentage(int pixelColor1, int pixelColor2) {
        Color color1 = new Color(pixelColor1);
        Color color2 = new Color(pixelColor2);
        return (int) ((colorDiff(color1.getRed(), color2.getRed()) + colorDiff(color1.getBlue(), color2.getBlue())
                + colorDiff(color1.getGreen(), color2.getGreen())) / 3.);
    }

    private int colorDiff(int color1, int color2) {
        return (int) ((Math.abs(color1 - color2) / 255.) * 100);
    }

    private boolean isImagesSizeEqual(BufferedImage imageToCompare1, BufferedImage imageToCompare2) {
        return imageToCompare1.getWidth() == imageToCompare2.getWidth()
                && imageToCompare1.getHeight() == imageToCompare2.getHeight();
    }

    private void printError(IOException e) {
        System.out.println("Exception thrown: " + e);
    }
}
