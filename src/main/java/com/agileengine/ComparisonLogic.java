package com.agileengine;

import java.io.File;

/**
 * Created by Dmitriy on 14.01.2016.
 */
public interface ComparisonLogic {
    void compareImages(File image1, File image2);
}
